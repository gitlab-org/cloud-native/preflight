FROM debian:bookworm AS build

ARG PREFLIGHT_VERSION=1.11.1
ARG YQ_VERSION=4.45.1
ARG PREFLIGHT_URL=https://github.com/redhat-openshift-ecosystem/openshift-preflight/releases/download/${PREFLIGHT_VERSION}/preflight-linux-amd64
ENV PREFLIGHT_VERSION=${PREFLIGHT_VERSION}

RUN apt update -y
RUN apt install -y curl
RUN curl -fsSL -o /usr/local/bin/preflight $PREFLIGHT_URL
RUN chmod 555 /usr/local/bin/preflight
RUN curl -fsSL -o /usr/local/bin/yq https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_linux_amd64
RUN chmod 755 /usr/local/bin/yq

FROM debian:bookworm-slim
COPY --from=build /usr/local/bin/* /usr/local/bin/
RUN apt update -y \
    && apt install -y --no-install-recommends podman ca-certificates jq && \
    rm -rf /var/lib/apt/lists/* && \
    echo 'event_logger = "file"' > /etc/containers/containers.conf
