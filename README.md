preflight container
===================

This is a packaging of the Red Hat preflight binary for certification pipelines.

GitHub repo where preflight source is located: https://github.com/redhat-openshift-ecosystem/openshift-preflight

Typical usage
-------------

```shell
❯ docker run -it --rm preflight:1.1.0 preflight check container registry.gitlab.com/gitlab-org/build/cng/gitlab-shell:2846-retool-certification-ubi8
time="2022-04-23T00:07:39Z" level=info msg="certification library version 1.1.0 <commit: f1628fcf489aed88711d6cac95c3f096217f9feb>"
time="2022-04-23T00:08:45Z" level=info msg="check completed: LayerCountAcceptable" result=PASSED
time="2022-04-23T00:08:45Z" level=info msg="check completed: HasNoProhibitedPackages" result=PASSED
time="2022-04-23T00:08:45Z" level=info msg="check completed: HasRequiredLabel" result=PASSED
time="2022-04-23T00:08:45Z" level=info msg="USER git:git specified that is non-root"
time="2022-04-23T00:08:45Z" level=info msg="check completed: RunAsNonRoot" result=PASSED
time="2022-04-23T00:08:46Z" level=info msg="check completed: BasedOnUbi" result=PASSED
time="2022-04-23T00:08:58Z" level=info msg="check completed: HasModifiedFiles" result=PASSED
time="2022-04-23T00:08:58Z" level=info msg="check completed: HasLicense" result=PASSED
time="2022-04-23T00:08:59Z" level=info msg="check completed: HasUniqueTag" result=PASSED
{
    "image": "registry.gitlab.com/gitlab-org/build/cng/gitlab-shell:2846-retool-certification-ubi8",
    "passed": true,
    "test_library": {
        "name": "github.com/redhat-openshift-ecosystem/openshift-preflight",
        "version": "1.1.0",
        "commit": "f1628fcf489aed88711d6cac95c3f096217f9feb"
    },
    "results": {
        "passed": [
            {
                "name": "LayerCountAcceptable",
                "elapsed_time": 0,
                "description": "Checking if container has less than 40 layers.  Too many layers within the container images can degrade container performance."
            },
            {
                "name": "HasNoProhibitedPackages",
                "elapsed_time": 124,
                "description": "Checks to ensure that the image in use does not include prohibited packages, such as Red Hat Enterprise Linux (RHEL) kernel packages."
            },
            {
                "name": "HasRequiredLabel",
                "elapsed_time": 1,
                "description": "Checking if the required labels (name, vendor, version, release, summary, description) are present in the container metadata."
            },
            {
                "name": "RunAsNonRoot",
                "elapsed_time": 1,
                "description": "Checking if container runs as the root user because a container that does not specify a non-root user will fail the automatic certification, and will be subject to a manual review before the container can be approved for publication"
            },
            {
                "name": "BasedOnUbi",
                "elapsed_time": 912,
                "description": "Checking if the container's base image is based upon the Red Hat Universal Base Image (UBI)"
            },
            {
                "name": "HasModifiedFiles",
                "elapsed_time": 11939,
                "description": "Checks that no files installed via RPM in the base Red Hat layer have been modified"
            },
            {
                "name": "HasLicense",
                "elapsed_time": 0,
                "description": "Checking if terms and conditions applicable to the software including open source licensing information are present. The license must be at /licenses"
            },
            {
                "name": "HasUniqueTag",
                "elapsed_time": 947,
                "description": "Checking if container has a tag other than 'latest', so that the image can be uniquely identified."
            }
        ],
        "failed": [],
        "errors": []
    }
}
time="2022-04-23T00:09:00Z" level=info msg="Preflight result: PASSED"
```

Using pipeline template
-----------------------

Since multiple projects use the `preflight` container for producing
certification results, there is a pipeline template so that a standardized
job can be used across the projects. There a few conditions that need to be
met in order to use the template.

1. The existing pipeline needs to have the `certification` stage added to
   the list of stages. The `certification` stage in most cases should be
   the last one to be listed.

1. The project should have the file `redhat-projects.yaml` at the top level
   that can be used to retrieve the Red Hat project identifier. This is not
   a hard requirement if one does not plan on submitting the certification
   results to Red Hat. There is also an option to specify the project
   identifier as a variable for the actual certification job. See the
   discussion below when the example job is created.

1. The project should have `REDHAT_API_TOKEN` defined as a pipeline variable.
   This is only necessary if certification results will be submitted to
   Red Hat.

Once these conditions have been met, then include the template with the
following in the project's `.gitlab-ci.yml` file.

```yaml
include:
  - project: 'gitlab-org/cloud-native/preflight'
    file: '/pipeline-template/certify.yaml'
```

The final task is to create a pipeline job for each image to be certified.
The following is an example from the CNG project that will scan the `sidekiq`
image built during the execution of the pipeline.

```yaml
gitlab-sidekiq-ee Certification:
  extends: .preflight-certification
  variables:
    CERTIFY_IMAGE: gitlab-sidekiq-ee
  needs:
    - gitlab-sidekiq-ee
```

`CERTIFY_IMAGE` is a required variable that give the base name of the image.
It is expected that the image will reside in the project's registry. The
tag name is taken from the running pipeline.

One may also specify `REDHAT_PROJECT_ID` as a variable for the Red Hat project
identifier if the certification results will be submitted to Red Hat. This
will remove the need to create the `redhat-projects.yaml` file in the root
of the project.

It is recommended that the certification job utilize the `needs` dependency
so that the certification job can be scheduled as soon as the image has been
built. This increases the performance of the overall pipeline rather than
waiting for all the prior stages to finish before starting the certification
stage.

Template variables
------------------

| Variable          | Required | Default                       | Description                                                 |
|-------------------|----------|-------------------------------|-------------------------------------------------------------|
| CERTIFY_IMAGE     |    Yes   |                               | Image name to certify                                       |
| CERTIFY_TAG       |    No    | `$CI_COMMIT_SHORT_SHA`        | Image tag to certify                                        |
| DOCKER_AUTH_FILE  |    No    | '/run/containers/0/auth.json' | File path in preflight container for Docker auth.json       |
| REDHAT_API_TOKEN  |    No    |                               | Token for authentication to Red Hat submission server       |
| REDHAT_PROJECT_ID |    No    |                               | Project ID assigned by Red Hat                              |
| REGISTRY_PASSWORD |    No    | `$CI_REGISTRY_PASSWORD`       | Allow registry token to be overridden with long lived token |
| REGISTRY_USER     |    No    | `$CI_REGISTRY_USER`           | Specify username for registry authentication                |
| OVERRIDE_CERTIFICATION_TAG | No |  | Override image tag sent for certification |

Format of `redhat-projects.yaml` file
-------------------------------------

If the `redhat-projects.yaml` file is being used instead of the `PROJECT_ID`
variable, then the structure of the file should be at a minimum as follows:

```yaml
<image name>:
  pid: <project ID>
```

Any more attributes for a project will be ignored.

The `image name` is the same image name provided as the `CERTIFY_IMAGE` to
the job.
